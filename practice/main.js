var fileSystem=require("fs");
var content="<h1>Hello World</h1>";
fileSystem.writeFile(__dirname+"/index.html",content,function(err){
    if(err){
        return console.log("ErrorCreated!")
    }else{
        return console.log("FileCreated!")
    }
});

var https=require("https");
var myPictureURL="https://studylinkclasses.com/img/logo@2x.jpg";
https.get(myPictureURL,function(data){
    data.pipe(fileSystem.createWriteStream(__dirname+"/sl_logo.jpg"));
})