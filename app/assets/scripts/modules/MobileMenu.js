class MobileMenu{
    constructor(){
        this.menuIcon=document.querySelector(".mobile-header-icon");
        this.mobileHeader=document.querySelector(".mobile-menu");
        this.events();
    }
    events(){
        this.menuIcon.addEventListener("click",()=>this.toggleMenu());
    }
    toggleMenu(){
        this.mobileHeader.classList.toggle("mobile-menu-active");
        this.menuIcon.classList.toggle("mobile-menu-icon-close");
    }
}

export default MobileMenu;