import $ from 'jquery';
import "../styles/style.css";
import MobileMenu from "./modules/MobileMenu";
import RevealOnScroll from "./modules/RevealOnScroll";
import SmoothScroll from './modules/SmoothScroll';
import ActiveLinks from './modules/ActiveLinks';
import Modal from './modules/Modal';
import "lazysizes";


if(module.hot){
    module.hot.accept();
}
//alert("Hello World");
//alert("Hello World 2");
console.log("app.js is ready!");
console.log("App.js newww");

let mobileMenu= new MobileMenu();
let revealOnScroll=new RevealOnScroll($('.section'),"40%");
new SmoothScroll();
new ActiveLinks();
new Modal();